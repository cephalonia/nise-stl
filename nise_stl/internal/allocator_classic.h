/**
 * @file allocator_classic.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2022-05-18
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef __NISE_STL_INTERNAL_ALLOCATOR_CLASSIC_H__
#define __NISE_STL_INTERNAL_ALLOCATOR_CLASSIC_H__

#include "new"
#include "utility"
#include "cstddef"

namespace nise {
/**
 * @brief default allocator.
 *
 * @tparam Type type to allocate.
 */
template <class Type>
class allocator;

// void is incomplete type.
template <>
class allocator<void> {
 public:
  using pointer = void *;
  using const_pointer = const void *;
  using value_type = void;

  template <class OtherType>
  struct rebind {
    using other = allocator<OtherType>;
  };
};

// (const)void is incomplete type.
template <>
class allocator<const void> {
 public:
  using pointer = const void *;
  using const_pointer = const void *;
  using value_type = const void;

  template <class OtherType>
  struct rebind {
    using other = allocator<OtherType>;
  };
};

template <typename Type>
class allocator {
 public:
  using value_type = Type;
  using size_type = std::size_t;
  using difference_type = ptrdiff_t;

  allocator() noexcept {};

  // covert constructor.
  template <typename OtherType>
  allocator(const allocator<OtherType> &) noexcept {}

  Type *allocate(std::size_t n) {
    if (n == 0) {
      return nullptr;
    }
    if (n > static_cast<std::size_t>(-1) / sizeof(Type)) {
      throw std::bad_array_new_length();
    }
    return static_cast<Type *>(::operator new(n * sizeof(Type)));
  }

  void deallocate(Type *ptr, std::size_t n) noexcept { ::operator delete(ptr); }

  // depracted members(C++20)
  using pointer = value_type *;
  using const_pointer = const value_type *;
  using reference = value_type &;
  using const_reference = const value_type &;
  template <typename OtherType>
  struct rebind {
    using other = allocator<OtherType>;
  };

  pointer address(reference obj) { return std::addressof(obj); }

  const_pointer address(const_reference obj) { return std::addressof(obj); }

  pointer allocate(size_type n, const void *) { return allocate(n); }

  size_type max_size() const noexcept {
    return static_cast<size_type>(-1) / sizeof(value_type);
  }

  template <typename OtherType, typename... Args>
  void construct(OtherType *ptr, Args &&...args) {
    ::new ((void *)ptr) OtherType(std::forward<Args>(args)...);
  }

  template <typename OtherType>
  void destory(OtherType *ptr) {
    ptr->~OtherType();
  }
};

template <typename Type>
class allocator<const Type> {
 public:
  using size_type = std::size_t;
  using difference_type = ptrdiff_t;
  using value_type = const Type;

  allocator() noexcept {}

  // covert constructor.
  template <typename OtherType>
  allocator(const allocator<OtherType> &) noexcept {}

  const Type *allocate(std::size_t n) {
    if (n == 0) {
      return nullptr;
    }
    if (n > static_cast<std::size_t>(-1) / sizeof(Type)) {
      throw std::bad_array_new_length();
    }
    return static_cast<Type *>(::operator new(n * sizeof(Type)));
  };

  void deallocate(const Type *ptr, std::size_t n) {
    ::operator delete(const_cast<Type *>(ptr));
  }

  // depracted members(C++20)
  using pointer = const Type *;
  using const_pointer = const Type *;
  using reference = const Type &;
  using const_reference = const Type &;

  template <typename OtherType>
  struct rebind {
    using other = allocator<OtherType>;
  };

  const_pointer address(const_reference obj) { return std::addressof(obj); }

  const_pointer allocate(size_type n, const void *) { return allocate(n); }

  size_type max_size() const noexcept {
    return static_cast<size_type>(-1) / sizeof(Type);
  }

  template <typename OtherType, typename... Args>
  void construct(OtherType *ptr, Args &&...args) {
    ::new ((void *)ptr) OtherType(std::forward<Args>(args)...);
  }

  template <typename OtherType>
  void destory(OtherType *ptr) {
    ptr->~OtherType();
  }
};
template <typename Type, typename OtherType>
bool operator==(const allocator<Type> &,
                const allocator<OtherType> &) noexcept {
  return true;
}

template <typename Type, typename OtherType>
bool operator!=(const allocator<Type> &, const allocator<OtherType>) noexcept {
  return false;
}
}  // namespace nise

#endif