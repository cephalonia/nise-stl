/**
 * @file allocator_cxx20.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2022-05-18
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef __NISE_STL_IINTERNAL_ALLOCATOR_CXX20_H_
#define __NISE_STL_IINTERNAL_ALLOCATOR_CXX20_H_
namespace nise {

template <typename Type>
class allocator {
 public:
  // member types.
  using value_type = Type;
  using size_type = std::size_t;
  using difference_type = std::pointer_traits;
  using propagate_on_container_move_assignment = std::true_type;

  // default constructor.
  constexpr allocator() noexcept = default;

  constexpr allocator(const allocator &other) noexcept = default;

  template <typename OtherType>
  constexpr allocator(const allocator<OtherType> &other) noexcept = default;
  // default destructor.

  constexpr ~allocator() = default;

  [[nodiscard]] constexpr Type *allocate(std::size_t n) {
    if (0 == n) {
      return nullptr;
    }
    if (n > std::numeric_limits<std::size_t>::max() / sizeof(Type)) {
      throw std::bad_array_new_length();
    }
    return static_cast<Type *>(::operator new(n * sizeof(Type)));
  }

  constexpr void deallocate(Type *p, std::size_t n) { ::operator delete(p); }
};

template <class T1, class T2>
constexpr bool operator==(const allocator<T1> &lhs,
                          const allocator<T2> &rhs) noexcept {
  return true;
}

template <class T1, class T2>
constexpr bool operator!=(const allocator<T1> &lhs,
                          const allocator<T2> &rhs) noexcept {
  return false;
}
}  // namespace nise

#endif