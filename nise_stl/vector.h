/**
 * @file vector.h
 * @author lipingan (lipingan.dev@outlook.com)
 * @brief
 * @version 0.1
 * @date 2022-05-08
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef __NISE_STL_VECTOR_H__
#define __NISE_STL_VECTOR_H__

#include "allocator.h"
#include "iterator.h"
#include "cstring"
#include "limits"

namespace nise {
static constexpr size_t kInitialVectorCapacity = 8;
template <typename Type, typename Allocator = allocator<Type>> class vector {
public:
  using allocator_type = Allocator;

  using value_type = Type;
  using size_type = std::size_t;
  using difference_type = std::ptrdiff_t;
  using reference = value_type &;
  using const_reference = const value_type &;
  using pointer = value_type *;
  using const_pointer = const value_type *;
  // iterators.
  using iterator = pointer_iterator<value_type>;
  using const_iterator = pointer_iterator<const_pointer>;

  // #1
  vector() : allocator_(), data_(nullptr), count_(0), limit_(0) {}

  // #2
  explicit vector(const allocator_type &a)
      : allocator_(a), data_(nullptr), count_(0), limit_(0) {}

  // #3
  vector(size_type count, const value_type &value,
         const allocator_type &alloc = allocator_type())
      : allocator_(alloc) {
    data_ = allocator_.allocate(count);
    count_ = count;
    limit_ = count;
    for (difference_type i = 0; i < count; ++i) {
      allocator_.construct(data_ + i, value);
    }
  }

  // #4
  explicit vector(size_type count,
                  const allocator_type &alloc = allocator_type())
      : allocator_(alloc) {
    count_ = count;
    limit_ = count;
    data_ = allocator_.allocate(count);
    for (difference_type i = 0; i < count; ++i) {
      allocator_.construct(data_ + i);
    }
  }

  // #5
  // template <class InputIterator>
  // vector(InputIterator first, InputIterator last,
  //        const Allocator &alloc = allocator_type())
  //     : allocator_(alloc) {
  //   count_ = last - first;
  //   data_ = allocator_.allocate(count_);
  //   limit_ = count_;
  //   for (size_t i = 0; i < count_; ++i) {
  //     allocator_.construct(data_[i], *(first++));
  //   }
  // }

  // #6
  vector(const vector &other)
      : allocator_(other.allocator_), count_(other.count_),
        limit_(other.limit_) {
    data_ = allocator_.allocate(count_);
    std::memcpy(data_, other.data_, count_ * sizeof(value_type));
  }
  // #7
  vector(const vector &other, const allocator_type &alloc)
      : allocator_(alloc), count_(other.count_), limit_(other.limit_) {
    data_ = allocator_.allocate(count_);
    std::memcpy(data_, other.data_, count_ * sizeof(value_type));
  }

  // #8
  vector(vector &&other) {
    std::swap(data_, other.data_);
    count_ = other.count_;
    limit_ = other.limit_;
    allocator_ = other.allocator_;
  }
  // #9
  vector(vector &&other, const allocator_type &alloc) : allocator_(alloc) {
    std::swap(data_, other.data_);
    count_ = other.count_;
    limit_ = other.limit_;
  }

  // #10
  vector(std::initializer_list<value_type> init,
         const allocator_type &alloca = allocator_type())
      : allocator_(alloca) {
    data_ = allocator_.allocate(init.size());
    count_ = init.size();
    limit_ = init.size();
    auto iter = init.begin();
    for (difference_type i = 0; iter != init.end(); ++i) {
      allocator_.construct(data_ + i, *(iter));
      ++iter;
    }
  }

  // capcacity
  bool empty() const noexcept { return count_ == 0; }

  size_type size() const noexcept { return count_; }

  size_type capacity() const noexcept { return limit_; }

  size_type max_size() const noexcept {
    return std::numeric_limits<difference_type>::max();
  }

  size_type reserve(size_type new_cap) {
    if (new_cap > limit_) {
      auto new_data = allocator_.allocate(new_cap);
      auto old_data = data_;
      std::memcpy(new_data, old_data, count_ * sizeof(value_type));
      data_ = new_data;
      allocator_.deallocate(old_data);
    }
  }

  void shrink_to_fit() {
    if (limit_ > count_) {
      auto new_data = allocator_.allocate(count_);
      std::memcpy(new_data, data_, count_ * sizeof(value_type));
    }
  }

private:
  allocator_type allocator_;
  pointer data_;
  size_type count_;
  size_type limit_;
};
} // namespace nise
#endif

/*
    vector synopsis

namespace std
{

template <class T, class Allocator = allocator<T> >
class vector
{
public:
    typedef T                                        value_type;
    typedef Allocator                                allocator_type;
    typedef typename allocator_type::reference       reference;
    typedef typename allocator_type::const_reference const_reference;
    typedef implementation-defined                   iterator;
    typedef implementation-defined                   const_iterator;
    typedef typename allocator_type::size_type       size_type;
    typedef typename allocator_type::difference_type difference_type;
    typedef typename allocator_type::pointer         pointer;
    typedef typename allocator_type::const_pointer   const_pointer;
    typedef std::reverse_iterator<iterator>          reverse_iterator;
    typedef std::reverse_iterator<const_iterator>    const_reverse_iterator;

    vector()
        noexcept(is_nothrow_default_constructible<allocator_type>::value);
    explicit vector(const allocator_type&);
    explicit vector(size_type n);
    explicit vector(size_type n, const allocator_type&); // C++14
    vector(size_type n, const value_type& value, const allocator_type& =
allocator_type()); template <class InputIterator> vector(InputIterator first,
InputIterator last, const allocator_type& = allocator_type()); vector(const
vector& x); vector(vector&& x)
        noexcept(is_nothrow_move_constructible<allocator_type>::value);
    vector(initializer_list<value_type> il);
    vector(initializer_list<value_type> il, const allocator_type& a);
    ~vector();
    vector& operator=(const vector& x);
    vector& operator=(vector&& x)
        noexcept(
             allocator_type::propagate_on_container_move_assignment::value ||
             allocator_type::is_always_equal::value); // C++17
    vector& operator=(initializer_list<value_type> il);
    template <class InputIterator>
        void assign(InputIterator first, InputIterator last);
    void assign(size_type n, const value_type& u);
    void assign(initializer_list<value_type> il);

    allocator_type get_allocator() const noexcept;

    iterator               begin() noexcept;
    const_iterator         begin()   const noexcept;
    iterator               end() noexcept;
    const_iterator         end()     const noexcept;

    reverse_iterator       rbegin() noexcept;
    const_reverse_iterator rbegin()  const noexcept;
    reverse_iterator       rend() noexcept;
    const_reverse_iterator rend()    const noexcept;

    const_iterator         cbegin()  const noexcept;
    const_iterator         cend()    const noexcept;
    const_reverse_iterator crbegin() const noexcept;
    const_reverse_iterator crend()   const noexcept;

    size_type size() const noexcept;
    size_type max_size() const noexcept;
    size_type capacity() const noexcept;
    bool empty() const noexcept;
    void reserve(size_type n);
    void shrink_to_fit() noexcept;

    reference       operator[](size_type n);
    const_reference operator[](size_type n) const;
    reference       at(size_type n);
    const_reference at(size_type n) const;

    reference       front();
    const_reference front() const;
    reference       back();
    const_reference back() const;

    value_type*       data() noexcept;
    const value_type* data() const noexcept;

    void push_back(const value_type& x);
    void push_back(value_type&& x);
    template <class... Args>
        reference emplace_back(Args&&... args); // reference in C++17
    void pop_back();

    template <class... Args> iterator emplace(const_iterator position, Args&&...
args); iterator insert(const_iterator position, const value_type& x); iterator
insert(const_iterator position, value_type&& x); iterator insert(const_iterator
position, size_type n, const value_type& x); template <class InputIterator>
        iterator insert(const_iterator position, InputIterator first,
InputIterator last); iterator insert(const_iterator position,
initializer_list<value_type> il);

    iterator erase(const_iterator position);
    iterator erase(const_iterator first, const_iterator last);

    void clear() noexcept;

    void resize(size_type sz);
    void resize(size_type sz, const value_type& c);

    void swap(vector&)
        noexcept(allocator_traits<allocator_type>::propagate_on_container_swap::value
|| allocator_traits<allocator_type>::is_always_equal::value);  // C++17

    bool __invariants() const;
};

template <class Allocator = allocator<T> >
class vector<bool, Allocator>
{
public:
    typedef bool                                     value_type;
    typedef Allocator                                allocator_type;
    typedef implementation-defined                   iterator;
    typedef implementation-defined                   const_iterator;
    typedef typename allocator_type::size_type       size_type;
    typedef typename allocator_type::difference_type difference_type;
    typedef iterator                                 pointer;
    typedef const_iterator                           const_pointer;
    typedef std::reverse_iterator<iterator>          reverse_iterator;
    typedef std::reverse_iterator<const_iterator>    const_reverse_iterator;

    class reference
    {
    public:
        reference(const reference&) noexcept;
        operator bool() const noexcept;
        reference& operator=(bool x) noexcept;
        reference& operator=(const reference& x) noexcept;
        iterator operator&() const noexcept;
        void flip() noexcept;
    };

    class const_reference
    {
    public:
        const_reference(const reference&) noexcept;
        operator bool() const noexcept;
        const_iterator operator&() const noexcept;
    };

    vector()
        noexcept(is_nothrow_default_constructible<allocator_type>::value);
    explicit vector(const allocator_type&);
    explicit vector(size_type n, const allocator_type& a = allocator_type()); //
C++14 vector(size_type n, const value_type& value, const allocator_type& =
allocator_type()); template <class InputIterator> vector(InputIterator first,
InputIterator last, const allocator_type& = allocator_type()); vector(const
vector& x); vector(vector&& x)
        noexcept(is_nothrow_move_constructible<allocator_type>::value);
    vector(initializer_list<value_type> il);
    vector(initializer_list<value_type> il, const allocator_type& a);
    ~vector();
    vector& operator=(const vector& x);
    vector& operator=(vector&& x)
        noexcept(
             allocator_type::propagate_on_container_move_assignment::value ||
             allocator_type::is_always_equal::value); // C++17
    vector& operator=(initializer_list<value_type> il);
    template <class InputIterator>
        void assign(InputIterator first, InputIterator last);
    void assign(size_type n, const value_type& u);
    void assign(initializer_list<value_type> il);

    allocator_type get_allocator() const noexcept;

    iterator               begin() noexcept;
    const_iterator         begin()   const noexcept;
    iterator               end() noexcept;
    const_iterator         end()     const noexcept;

    reverse_iterator       rbegin() noexcept;
    const_reverse_iterator rbegin()  const noexcept;
    reverse_iterator       rend() noexcept;
    const_reverse_iterator rend()    const noexcept;

    const_iterator         cbegin()  const noexcept;
    const_iterator         cend()    const noexcept;
    const_reverse_iterator crbegin() const noexcept;
    const_reverse_iterator crend()   const noexcept;

    size_type size() const noexcept;
    size_type max_size() const noexcept;
    size_type capacity() const noexcept;
    bool empty() const noexcept;
    void reserve(size_type n);
    void shrink_to_fit() noexcept;

    reference       operator[](size_type n);
    const_reference operator[](size_type n) const;
    reference       at(size_type n);
    const_reference at(size_type n) const;

    reference       front();
    const_reference front() const;
    reference       back();
    const_reference back() const;

    void push_back(const value_type& x);
    template <class... Args> reference emplace_back(Args&&... args);  // C++14;
reference in C++17 void pop_back();

    template <class... Args> iterator emplace(const_iterator position, Args&&...
args);  // C++14 iterator insert(const_iterator position, const value_type& x);
    iterator insert(const_iterator position, size_type n, const value_type& x);
    template <class InputIterator>
        iterator insert(const_iterator position, InputIterator first,
InputIterator last); iterator insert(const_iterator position,
initializer_list<value_type> il);

    iterator erase(const_iterator position);
    iterator erase(const_iterator first, const_iterator last);

    void clear() noexcept;

    void resize(size_type sz);
    void resize(size_type sz, value_type x);

    void swap(vector&)
        noexcept(allocator_traits<allocator_type>::propagate_on_container_swap::value
|| allocator_traits<allocator_type>::is_always_equal::value);  // C++17 void
flip() noexcept;

    bool __invariants() const;
};

template <class InputIterator, class Allocator = allocator<typename
iterator_traits<InputIterator>::value_type>> vector(InputIterator,
InputIterator, Allocator = Allocator())
   -> vector<typename iterator_traits<InputIterator>::value_type, Allocator>;

template <class Allocator> struct hash<std::vector<bool, Allocator>>;

template <class T, class Allocator> bool operator==(const vector<T,Allocator>&
x, const vector<T,Allocator>& y); template <class T, class Allocator> bool
operator< (const vector<T,Allocator>& x, const vector<T,Allocator>& y); template
<class T, class Allocator> bool operator!=(const vector<T,Allocator>& x, const
vector<T,Allocator>& y); template <class T, class Allocator> bool operator>
(const vector<T,Allocator>& x, const vector<T,Allocator>& y); template <class T,
class Allocator> bool operator>=(const vector<T,Allocator>& x, const
vector<T,Allocator>& y); template <class T, class Allocator> bool
operator<=(const vector<T,Allocator>& x, const vector<T,Allocator>& y);

template <class T, class Allocator>
void swap(vector<T,Allocator>& x, vector<T,Allocator>& y)
    noexcept(noexcept(x.swap(y)));

template <class T, class Allocator, class U>
typename vector<T, Allocator>::size_type
erase(vector<T, Allocator>& c, const U& value);       // C++20
template <class T, class Allocator, class Predicate>
typename vector<T, Allocator>::size_type
erase_if(vector<T, Allocator>& c, Predicate pred);    // C++20

}  // std

*/