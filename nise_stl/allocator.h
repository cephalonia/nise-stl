/**
 * @file allocator.h
 * @author lipingan (lipingan.dev@outlook.com)
 * @brief
 * @version 0.1
 * @date 2022-05-08
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef __NISE_STL_ALLOCATOR_H__
#define __NISE_STL_ALLOCATOR_H__

#include "internal/allocator_classic.h"
#endif