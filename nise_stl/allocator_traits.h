/**
 * @file allocator_traits.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2022-05-18
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef __NISE_STL_ALLOCATOR_TRAITS_H__
#define __NISE_STL_ALLOCATOR_TRAITS_H__


#include "new"

namespace nise {
// Universal Construction.
template <typename Type, typename... Args>
void construct(Type *const p, Args &&...args) {
  ::new (static_cast<void *>(p)) Type(std::forward<Args>(args)...);
}

// Universal Destruction.
template <typename Type>
void destruct(Type *const p) {
  p->~Type();
}
}  // namespace nise

#endif
