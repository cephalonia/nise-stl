/**
 * @file iterator_traits.h
 * @author lipingan (lipingan.dev@outlook.com)
 * @brief
 * @version 0.1
 * @date 2022-05-08
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef __NISE_STL_ITERATOR_TRAITS_H__
#define __NISE_STL_ITERATOR_TRAITS_H__
#include "iterator"
#include "type_traits"

namespace nise {
template <typename Iterator> struct iterator_traits {
  using value_type = typename Iterator::value_type;
  using pointer = typename Iterator::pointer;
  using reference = typename Iterator::reference;
  using difference_type = typename Iterator::difference_type;
  using iterator_category = typename Iterator::iterator_category;
};

template <typename Type> struct iterator_traits<Type *> {
  using value_type = typename std::remove_cv<Type>::type;
  using pointer = value_type *;
  using reference = value_type &;
  using difference_type = std::ptrdiff_t;
  using iterator_category = std::random_access_iterator_tag;
};

} // namespace nise

#endif