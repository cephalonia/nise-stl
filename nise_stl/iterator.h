/**
 * @file iterator_impl.h
 * @author lipingan (lipingan.dev@outlook.com)
 * @brief
 * @version 0.1
 * @date 2022-05-08
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef __NISE_STL_ITERATOR_H__
#define __NISE_STL_ITERATOR_H__
#include "iterator_traits.h"
namespace nise {
template <typename Category, typename Type, typename Distance = ptrdiff_t,
          typename Pointer = Type *, typename Reference = Type &>
struct iterator {
  using value_type = Type;
  using difference_type = Distance;
  using pointer = Pointer;
  using reference = Reference;
  using iterator_category = Category;
};

template <typename Pointer> struct pointer_iterator {
  using value_type = typename iterator_traits<Pointer>::value_type;
  using pointer = typename iterator_traits<Pointer>::pointer;
  using reference = typename iterator_traits<Pointer>::reference;
  using difference_type = typename iterator_traits<Pointer>::difference_type;
  using iterator_category =
      typename iterator_traits<Pointer>::iterator_category;

  pointer_iterator(pointer ptr) : ptr_(ptr) {}

  pointer_iterator(const pointer_iterator &ptr) : ptr_(ptr) {}

  pointer_iterator &operator=(const pointer_iterator &ptr) {
    auto tmp = ptr;
    std::swap(tmp.ptr_, ptr);
    return *this;
  }

  pointer operator->() { return ptr_; }

  reference operator*() { return *ptr_; }

  pointer_iterator &operator++() {
    ++ptr_;
    return *this;
  }

  pointer_iterator operator++(int) {
    pointer_iterator tmp(*this);
    ++(*this);
    return tmp;
  }

  pointer_iterator &operator--() {
    --ptr_;
    return *this;
  }

  pointer_iterator operator--(int) {
    pointer_iterator tmp(*this);
    --(*this);
    return tmp;
  }

  pointer_iterator operator+(difference_type diff) {
    pointer_iterator tmp(*this);
    tmp.ptr_ += diff;
    return tmp;
  }

  pointer_iterator &operator+=(difference_type diff) {
    ptr_ += diff;
    return *this;
  }

  pointer_iterator operator-(difference_type diff) {
    pointer_iterator tmp(*this);
    tmp.ptr_ -= diff;
    return tmp;
  }

  pointer_iterator &operator-=(difference_type diff) {
    ptr_ -= diff;
    return *this;
  }

  pointer_iterator &operator[](difference_type index) { return ptr_[index]; }

  pointer base() { return ptr_; }

private:
  pointer ptr_;

  template <typename OtherType> friend struct pointer_iterator;
  template <typename Type, typename Alloc> friend class vector;
};
} // namespace nise

#endif