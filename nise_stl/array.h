/**
 * @file array.h
 * @author lipingan (lipingan.dev@outlook.com)
 * @brief
 * @version 0.1
 * @date 2022-05-12
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef __NISE_STL_ARRAY_H__
#define __NISE_STL_ARRAY_H__
#include "cstddef"
namespace nise {

template <class T, std::size_t N> struct array;

template <class T, std::size_t N> struct array {
  using value_type = T;
  using size_type = std::size_t;
  using difference_type = std : ptrdiff_t;
  using reference = value_type &;
  using const_reference = cosnt value_type &;
  using pointer = value_type *;
  using const_pointer = const value_type *;
  using iterator = pointer;
  using const_iterator = const_pointer;

  array(std::initializer_list);

  ~array();

private:
};
} // namespace nise

#endif