/**
 * @file vector_unittest.cpp
 * @author lipingan (lipingan.dev@outlook.com)
 * @brief
 * @version 0.1
 * @date 2022-05-14
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "nise_stl/vector.h"
#include "test_object.h"
#include "gtest/gtest.h"
namespace nise {
TEST(vecotr, constructor_no_1) {
  vector<int> vector;
  EXPECT_EQ(vector.data_, nullptr);
  EXPECT_EQ(vector.count_, 0);
  EXPECT_EQ(vector.limit_, 0);
}

TEST(vecotr, constructor_no_2) {
  allocator<int> alloca;
  vector<int> vector(alloca);
  EXPECT_EQ(vector.data_, nullptr);
  EXPECT_EQ(vector.count_, 0);
  EXPECT_EQ(vector.limit_, 0);
}

TEST(vecotr, constructor_no_3) {
  size_t vec_size = 1024;
  vector<int> vector(vec_size, vec_size);
  EXPECT_EQ(vector.count_, vec_size);
  EXPECT_EQ(vector.limit_, vec_size);
  for (size_t i = 0; i < vec_size; i++) {
    EXPECT_EQ(vector.data_[i], vec_size);
  }
}

TEST(vecotr, constructor_no_4) {
  size_t vec_size = 1024;
  vector<int> vector(vec_size);
  EXPECT_EQ(vector.count_, vec_size);
  EXPECT_EQ(vector.limit_, vec_size);
}

TEST(vecotr, constructor_no_5) {}

TEST(vector, constructor_no_6) {
  size_t vec_size = 1024;
  vector<int> vec(vec_size, vec_size);
  vector<int> other(vec);
}

TEST(vector, constructor_no_7) {
  size_t vec_size = 1024;
  vector<int> vec(vec_size, vec_size);
  vector<int> other(vec, allocator<int>{});
}

TEST(vector, constructor_no_8) {
  size_t vec_size = 1024;
  vector<int> vec(vec_size, vec_size);
  vector<int> other(std::move(vec));
}

TEST(vector, constructor_no_9) {
  size_t vec_size = 1024;
  vector<int> vec(vec_size, vec_size);
  vector<int> other(std::move(vec), allocator<int>{});
}

TEST(vector, constructor_no_10) {
  vector<int> vec{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  EXPECT_EQ(vec.count_, 10);
  EXPECT_EQ(vec.limit_, 10);
  for (size_t i = 0; i < 10; ++i) {
    EXPECT_EQ(vec.data_[i], i);
  }
}

TEST(vector, size_0) {
  vector<int> vec{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  EXPECT_EQ(vec.size(), 10);
  EXPECT_EQ(vec.empty(), false);
}

TEST(vector, size_1) {
  vector<int> vec{};
  EXPECT_EQ(vec.size(), 0);
  EXPECT_EQ(vec.empty(), true);
}

TEST(vector, max_size_0) {
  vector<char> vec{};
  EXPECT_EQ(vec.max_size(), 9223372036854775807);
}

} // namespace nise
