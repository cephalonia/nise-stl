/**
 * @file iterator_traits_unittest.cpp
 * @author lipingan (lipingan.dev@outlook.com)
 * @brief
 * @version 0.1
 * @date 2022-05-12
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "nise_stl/iterator_traits.h"
#include "gtest/gtest.h"
namespace nise {
TEST(iterator_trait, pointer) {
  bool result = std::is_same<int, iterator_traits<int *>::value_type>::value;
  EXPECT_TRUE(result);
  result = std::is_same<int *, iterator_traits<int *>::pointer>::value;
  EXPECT_TRUE(result);
  result = std::is_same<int &, iterator_traits<int *>::reference>::value;
  EXPECT_TRUE(result);
  result = std::is_same<std::random_access_iterator_tag,
                        iterator_traits<int *>::iterator_category>::value;
  EXPECT_TRUE(result);
}

TEST(iterator_trait, const_pointer) {
  bool result =
      std::is_same<int, iterator_traits<const int *>::value_type>::value;
  EXPECT_TRUE(result);
  result = std::is_same<int *, iterator_traits<const int *>::pointer>::value;
  EXPECT_TRUE(result);
  result = std::is_same<int &, iterator_traits<const int *>::reference>::value;
  EXPECT_TRUE(result);
  result = std::is_same<std::random_access_iterator_tag,
                        iterator_traits<int *>::iterator_category>::value;
  EXPECT_TRUE(result);
}
} // namespace nise
