/**
 * @file test_object.cpp
 * @author lipingan (lipingan.dev@outlook.com)
 * @brief
 * @version 0.1
 * @date 2022-05-14
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "test_object.h"
#include "iostream"

namespace nise {
TestObject::TestObject() {
  std::cout << "TestObject constructed!" << std::endl;
}

TestObject::TestObject(int i, double fp) : i_(i), fp_(fp) {
  std::cout << "TestObject(int i , double fp) constructed!" << std::endl;
}

TestObject::~TestObject() { std::cout << "TestObject destroyed!" << std::endl; }

TestObject::TestObject(const TestObject &other) {
  std::cout << "TestObject copy-constructed!" << std::endl;
}

TestObject &TestObject::operator=(const TestObject &other) {
  std::cout << "TestObject copy-assigment!" << std::endl;
}
} // namespace nise
