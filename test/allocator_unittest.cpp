/**
 * @file allocator_unittest.cpp
 * @author lipingan (lipingan.dev@outlook.com)
 * @brief
 * @version 0.1
 * @date 2022-05-14
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "nise_stl/allocator.h"

#include "test_object.h"
#include "gtest/gtest.h"
namespace nise {
TEST(allocator, traits_void) {
  bool result = false;
  result = std::is_same<allocator<void>::value_type, void>::value;
  EXPECT_TRUE(result);
  result = std::is_same<allocator<void>::pointer, void *>::value;
  EXPECT_TRUE(result);
  result = std::is_same<allocator<void>::const_pointer, const void *>::value;
  EXPECT_TRUE(result);
}

TEST(allocator, traits_const_void) {
  bool result = false;
  result = std::is_same<allocator<const void>::value_type, const void>::value;
  EXPECT_TRUE(result);
  result = std::is_same<allocator<const void>::pointer, const void *>::value;
  EXPECT_TRUE(result);
  result =
      std::is_same<allocator<const void>::const_pointer, const void *>::value;
  EXPECT_TRUE(result);
}

TEST(allocator, traits_int) {
  bool result = false;
  result = std::is_same<allocator<int>::value_type, int>::value;
  EXPECT_TRUE(result);
  result = std::is_same<allocator<int>::pointer, int *>::value;
  EXPECT_TRUE(result);
  result = std::is_same<allocator<int>::const_pointer, const int *>::value;
  EXPECT_TRUE(result);
}

TEST(allocator, traits_const_int) {
  bool result = false;
  result = std::is_same<allocator<const int>::value_type, const int>::value;
  EXPECT_TRUE(result);
  result = std::is_same<allocator<const int>::pointer, const int *>::value;
  EXPECT_TRUE(result);
  result =
      std::is_same<allocator<const int>::const_pointer, const int *>::value;
  EXPECT_TRUE(result);
}

TEST(allocator, traits_int_array) {
  bool result = false;
  result = std::is_same<allocator<int[5]>::value_type, int[5]>::value;
  EXPECT_TRUE(result);
  result = std::is_same<allocator<int[5]>::pointer, int(*)[5]>::value;
  EXPECT_TRUE(result);
  result =
      std::is_same<allocator<int[5]>::const_pointer, const int(*)[5]>::value;
  EXPECT_TRUE(result);
}

TEST(allocator, traits_const_int_array) {
  bool result = false;
  result =
      std::is_same<allocator<const int[5]>::value_type, const int[5]>::value;
  EXPECT_TRUE(result);
  result =
      std::is_same<allocator<const int[5]>::pointer, const int(*)[5]>::value;
  EXPECT_TRUE(result);
  result = std::is_same<allocator<const int[5]>::const_pointer,
                        const int(*)[5]>::value;
  EXPECT_TRUE(result);
}

TEST(allocator, allocate_int) {
  allocator<int> a;
  size_t array_size = 65536;
  int *ptr = a.allocate(array_size);
  for (size_t i = 0; i < array_size; ++i) {
    ptr[i] = i;
  }
  for (size_t i = 0; i < array_size; ++i) {
    EXPECT_EQ(ptr[i], i);
  }
  a.deallocate(ptr, array_size);
}

TEST(allocator, construct_int) {
  allocator<int> a;
  size_t array_size = 65536;
  int *ptr = a.allocate(array_size);
  for (size_t i = 0; i < array_size; ++i) {
    a.construct(ptr + i, i);
  }
  for (size_t i = 0; i < array_size; ++i) {
    EXPECT_EQ(ptr[i], i);
  }
  a.deallocate(ptr, array_size);
}

TEST(allocator, construct_test_object) {
  allocator<TestObject> a;
  size_t array_size = 4;
  auto *ptr = a.allocate(array_size);
  for (size_t i = 0; i < array_size; ++i) {
    a.construct(ptr + i, 42, 42.0);
  }
  a.deallocate(ptr, array_size);
}

TEST(allocator, destroy_test_object) {
  allocator<TestObject> a;
  size_t array_size = 4;
  auto *ptr = a.allocate(array_size);
  for (size_t i = 0; i < array_size; ++i) {
    a.construct(ptr + i, 42, 42.0);
  }
  for (size_t i = 0; i < array_size; ++i) {
    a.destory(ptr + i);
  }
  a.deallocate(ptr, array_size);
}
} // namespace nise
