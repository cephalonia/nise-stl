/**
 * @file test_object.h
 * @author lipingan (lipingan.dev@outlook.com)
 * @brief
 * @version 0.1
 * @date 2022-05-14
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef __NISE_STL_UNITTEST_TEST_OBJECT_H__
#define __NISE_STL_UNITTEST_TEST_OBJECT_H__
namespace nise {
struct TestObject {

  TestObject();

  TestObject(int i, double fp);

  ~TestObject();

  TestObject(const TestObject &);

  TestObject &operator=(const TestObject &);

  int i_{42};
  double fp_{42.0};
};

} // namespace nise

#endif